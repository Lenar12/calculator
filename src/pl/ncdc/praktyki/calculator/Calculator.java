package pl.ncdc.praktyki.calculator;

public class Calculator {
	
	public float add(float first, float second){
		return first+second;
	}
	
	public float substract(float first, float second){
		return first-second;
	}
	
	public float divide(float first, float second){
		return first/second;
	}
	
	public float multiply(float first, float second){
		return first*second;
	}
	
}
