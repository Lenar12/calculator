package pl.ncdc.praktyki.calculator;

public class CalculatorController extends Calculator {
	public float calculate(float firstNumber, float secondNumber, String symbol){
		switch(symbol){
		case "+":
			return add(firstNumber, secondNumber);
		case "-":
			return substract(firstNumber, secondNumber);
		case "*":
			return multiply(firstNumber, secondNumber);
		case "/":
			return divide(firstNumber, secondNumber);
		default:
			System.out.println("Nie obsłużono takiego działania");
			return 0;
		}
	}
}
